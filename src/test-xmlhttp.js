import { LitElement, html, css } from 'lit-element';

class TestXmlhttp extends LitElement {

    static get styles() {
        return css`
      :host {
        display: block;
      }
    `;
    }

    static get properties() {
        return {
            planet: { type: Object }
        };
    }

    constructor() {
        super();
        this.cargarPlaneta();
    }

    render() {
        return html`
      <p><code>${this.planet}</code></p>
    `;
    }

    cargarPlaneta() {
        //xmlHttpRequest
        var req = new XMLHttpRequest();
        req.open('GET', 'https://swapi.dev/api/planets/1/', true);
        req.onreadystatechange = ((aEvt) => {
            if (req.readyState === 4) { //4 es el codigo que indica que se terminaron de bajar todos los paquetes de la petición
                if (req.status === 200) {
                    //funciono
                    this.planet = req.responseText;
                    this.requestUpdate();
                } else {
                    //ha fallado la petición
                    alert('Error llamando a REST');
                }
            }
        });
        req.send(null);
    }
}

customElements.define('test-xmlhttp', TestXmlhttp);